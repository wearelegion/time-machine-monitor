#!/usr/bin/env bash

last_backup="$(tmutil latestbackup)"
backup_time="${last_backup##*/}"
backup_date="${backup_time%-*}"
compare_date=$(gdate -d "$date -3 days" +"%Y-%m-%d")
# compare_date="2020-03-08"
compare_date_seconds="$(date -j -f "%F" "$compare_date" +%s)"
backup_date_seconds="$(date -j -f "%F" "$backup_date" +%s)"

function send_email () {
  echo "$1" | mail -s "backup" martin@prescriba.com 
}

if [[ "$backup_date_seconds" -lt "$compare_date_seconds"  ]]; then
  if [[ ! -f /tmp/backup-notification-send ]]; then
    send_email "we didn't have a backup today - last backup $last_backup"
    touch /tmp/backup-notification-send
  fi
else
  if [[ -f /tmp/backup-notification-send ]]; then
    rm /tmp/backup-notification-send
  fi
fi
