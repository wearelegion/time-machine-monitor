#!/usr/bin/env bash

set -e # Exit on error

if ! brew --help > /dev/null; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi
brew list coreutils > /dev/null || brew install coreutils

[ ! -d ~/.crontab-scripts ] && mkdir ~/.crontab-scripts
cp tm-backup-monitor.sh ~/.crontab-scripts

add_to_crontab () {
  (crontab -l 2>/dev/null; echo "$1") | crontab -
}

if ! crontab -l | grep -q tm-backup-monitor.sh; then
  add_to_crontab "5 * * * * source ~/.crontab-scripts/tm-backup-monitor.sh > /dev/null"
else
  echo "crontab already installed"
fi

