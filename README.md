Tool for making sure that time machine backups are running.

Clone this repo to target computer, e.g. in /tmp.

Install with `make install`. Test with `make test`.

Basically, asks every hour whether there has been a backup made today. If not, send email.
